#include <unistd.h>
#include <iostream>
#include <exception>
#include <fstream>
#include <string>
#include <cstring>
#include "extractor.h"

using std::cerr;
using std::cout;
using std::exception;
using std::ofstream;
using std::string;

#define dir_delimter '/'

Extractor::Extractor(const char* archiveName)
{
    zip_file = zip_open(archiveName, 0, &err);
    if (!zip_file) {
        cerr << archiveName << " file cannot be opened" << "\n";
    }
    files_total = zip_get_num_files(zip_file);
}

Extractor::~Extractor()
{
    zip_close(zip_file);
}

const char* Extractor::readCompressedFile()
{
    char* contents = new char[file_info.size];
    file_in_zip = zip_fopen_encrypted(zip_file, file_info.name, 0, password);
    if (!file_in_zip) {
        cerr << file_info.name << " file cannot be opened" << "\n";
    }
    zip_fread(file_in_zip, contents, file_info.size);
    zip_fclose(file_in_zip);
    return contents;
}

void Extractor::writeFile(const char* contents)
{
    if (!ofstream(catalog + file_info.name).write(contents, file_info.size)) {
        cerr << "Error writing file: " << file_info.name << '\n';
    }
}

void Extractor::extractFile()
{
    string directory = "mkdir -p " + catalog;
    system(directory.c_str());
    for (int i = 0; i < files_total; ++i) {
        zip_stat_index(zip_file, i, 0, &file_info);
        if (file_info.name[strlen(file_info.name) - 1] == dir_delimter) {
            string path = directory + file_info.name;
            const int dir_err = system(path.c_str());
            if (dir_err == -1) {
                cerr << "File cannot be created in " << path << " folder" << "\n";
            }
            continue;
        }
        emit send(file_info.name);
        const char* contents = readCompressedFile();
        writeFile(contents);
        delete contents;
    }
    //execl("/home/evgenii/aaa/main.cpp");
    emit exit();
}
