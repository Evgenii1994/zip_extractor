#ifndef EXTRACTOR_H
#define EXTRACTOR_H

#include <zip.h>
#include <string>
#include <QObject>
#include <QString>
#include <QThread>


class Extractor : public QObject
{
    Q_OBJECT

public:
    Extractor(const char* archiveName);
    ~Extractor();
    const char* readCompressedFile();
    void writeFile(const char* contents);
public slots:
    void extractFile();

signals:
    void send(QString);
    void exit();

private:
    struct zip* zip_file;
    struct zip_file* file_in_zip;
    struct zip_stat file_info;
    int err;
    int files_total;
    const char* password = "12345";
    std::string catalog = "/home/evgenii/aaa/";
};

#endif // EXTRACTOR_H
