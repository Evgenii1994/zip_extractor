#include "extractor.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QThread* thread = new QThread;
    const char* archiveName = "secure.zip";
    Extractor* zipExt = new Extractor(archiveName);
    zipExt->moveToThread(thread);
    connect(zipExt, SIGNAL(send(QString)), this, SLOT(update(QString)));
    connect(thread, SIGNAL(started()), zipExt, SLOT(extractFile()));
    connect(zipExt, SIGNAL(exit()), this, SLOT(close()));
    thread->start();
}

void MainWindow::update(QString str)
{
    ui->textEdit->setText(str);
}
